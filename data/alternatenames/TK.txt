1547372	4031073		Union Group						
1547373	4031073		Tokelau Islands Dependency						
1547374	4031073		Tokalau Islands						
1547375	4031073		Tokelan Islands						
1547376	4031073		Tokolau						
1547377	4031073		Union Islands						
1547378	4031073		Tokelau Group						
1627192	4031073	ca	Illes Tokelau						
4315685	4031073		Tokelau Islands						
1627190	4031074	ca	Tokelau	1					
1627191	4031074	oc	Tokelau						
2417942	4031074	am	ቶክላው	1					
2417943	4031074	ar	توكيلو	1					
2417944	4031074	be	Такелау	1					
2417945	4031074	bg	Токелау	1					
2417946	4031074	bn	টোকেলাউ	1					
2417947	4031074	cs	Tokelau	1					
2417948	4031074	cy	Tokelau	1					
2417949	4031074	da	Tokelau	1					
2417950	4031074	de	Tokelau	1					
2417951	4031074	el	Τοκελάου	1					
2417952	4031074	en	Tokelau	1					
2417953	4031074	es	Tokelau	1					
2417954	4031074	et	Tokelau	1					
2417955	4031074	eu	Tokelau	1					
2417956	4031074	fa	توکلائو	1					
2417957	4031074	fi	Tokelau	1					
2417958	4031074	fr	Tokelau	1					
2417959	4031074	ga	Tócalá	1					
2417960	4031074	gl	Tokelau	1					
2417961	4031074	he	טוקלאו	1					
2417962	4031074	hi	तोकेलाउ	1					
2417963	4031074	hr	Tokelau	1					
2417964	4031074	hu	Tokelau	1					
2417965	4031074	ia	Tokelau	1					
2417966	4031074	id	Tokelau	1					
2417967	4031074	is	Tókelá	1					
2417968	4031074	it	Tokelau	1					
2417969	4031074	ja	トケラウ	1					
2417970	4031074	ko	토켈라우	1					
2417971	4031074	lo	ໂຕເກເລົາ	1					
2417972	4031074	lt	Tokelau	1					
2417973	4031074	lv	Tokelau	1					
2417974	4031074	mk	Токелау	1					
2417975	4031074	ms	Tokelau	1					
2417976	4031074	mt	it-Tokelau	1					
2417977	4031074	nb	Tokelau	1					
2417978	4031074	nl	Tokelau	1					
2417979	4031074	nn	Tokelau	1					
2417980	4031074	pl	Tokelau	1					
2417981	4031074	pt	Tokelau	1					
2417982	4031074	ro	Tokelau	1					
2417983	4031074	ru	Токелау	1					
2417984	4031074	se	Tokelau	1					
2417985	4031074	sk	Tokelau	1					
2417986	4031074	sl	Tokelau	1					
2417987	4031074	sr	Токелау	1					
2417988	4031074	sv	Tokelauöarna	1					
2417989	4031074	th	โตเกเลา	1					
2417990	4031074	to	Tokelau	1					
2417991	4031074	tr	Tokelau	1					
2417992	4031074	uk	Токелау	1					
2417993	4031074	ur	ٹوکیلاؤ	1					
2417994	4031074	vi	Tokelau	1					
2417995	4031074	zh	托克劳	1					
2920392	4031074	link	https://en.wikipedia.org/wiki/Tokelau						
3056804	4031074	link	https://ru.wikipedia.org/wiki/%D0%A2%D0%BE%D0%BA%D0%B5%D0%BB%D0%B0%D1%83						
4315686	4031074		Tokelau						
7091207	4031074	af	Tokelau	1					
7091208	4031074	ak	Tokelau	1					
7091209	4031074	az	Tokelau	1					
7091210	4031074	bm	Tokelo	1					
7091211	4031074	br	Tokelau	1					
7091212	4031074	bs	Tokelau	1					
7091213	4031074	ee	Tokelau nutome	1					
7091214	4031074	ff	Tokelaaw	1					
7091215	4031074	fo	Tokelau	1					
7091216	4031074	gu	ટોકેલાઉ	1					
7091217	4031074	ha	Takelau	1					
7091218	4031074	ki	Tokelau	1					
7091219	4031074	kl	Tokelau	1					
7091220	4031074	kn	ಟೊಕೆಲಾವ್	1					
7091221	4031074	lg	Tokelawu	1					
7091222	4031074	ln	Tokelau	1					
7091223	4031074	lu	Tokelau	1					
7091224	4031074	mg	Tokelao	1					
7091225	4031074	ml	ടോക്കെലൂ	1					
7091226	4031074	mr	तोकेलाउ	1					
7091227	4031074	nd	Thokelawu	1					
7091228	4031074	ne	तोकेलाउ	1					
7091229	4031074	or	ଟୋକେଲାଉ	1					
7091230	4031074	rm	Tokelau	1					
7091231	4031074	rn	Tokelawu	1					
7091232	4031074	sg	Tokelau	1					
7091233	4031074	si	ටොකලාවු	1					
7091234	4031074	sn	Tokelau	1					
7091235	4031074	so	Tokelaaw	1					
7091236	4031074	sw	Tokelau	1					
7091237	4031074	ta	டோகேலோ	1					
7091238	4031074	te	టోకెలావ్	1					
7091239	4031074	ti	ቶክላው	1					
7091240	4031074	yo	Tokelau	1					
7091241	4031074	zu	i-Tokelau	1					
13190095	4031074	tkl	Tokelau						
16925872	4031074	zh-Hant	托克勞群島	1					
16931540	4031074	as	টোকেলাউ	1					
16931541	4031074	ce	Токелау	1					
16931542	4031074	dz	ཏོ་ཀེ་ལའུ་ མཚོ་གླིང	1					
16931543	4031074	fy	Tokelau	1					
16931544	4031074	gd	Tokelau	1					
16931545	4031074	hy	Տոկելաու	1					
16931546	4031074	ig	Tokelau	1					
16931547	4031074	jv	Tokelau	1					
16931548	4031074	ka	ტოკელაუ	1					
16931549	4031074	kk	Токелау	1					
16931550	4031074	km	តូខេឡៅ	1					
16931551	4031074	ks	توکیلاو	1					
16931552	4031074	ku	Tokelau	1					
16931553	4031074	ky	Токелау	1					
16931554	4031074	lb	Tokelau	1					
16931555	4031074	mn	Токелау	1					
16931556	4031074	my	တိုကလောင်	1					
16931557	4031074	no	Tokelau	1					
16931558	4031074	pa	ਟੋਕੇਲਾਉ	1					
16931559	4031074	ps	توکیلو	1					
16931560	4031074	qu	Tokelau	1					
16931561	4031074	sd	ٽوڪلائو	1					
16931562	4031074	sq	Tokelau	1					
16931563	4031074	tg	Токелау	1					
16931564	4031074	tk	Tokelau	1					
16931565	4031074	tt	Токелау	1					
16931566	4031074	ug	توكېلاۋ	1					
16931567	4031074	uz	Tokelau	1					
16931568	4031074	wo	Tokoloo	1					
1547381	4031090		Nukuno						
1547382	4031090		Duke of Clarence Island						
1547383	4031090		Nukunono						
4315689	4031090		Nukunonu Atoll						
1547384	4031091		Nuko Nono Island						
1547385	4031091		Nukunono						
4315690	4031091		Nukunonu						
5424442	4031091	link	https://en.wikipedia.org/wiki/Nukunonu						
7588760	4031091	ur	نوکونونو						
7588761	4031091	gl	Atol Nukunono						
7588762	4031091	uk	Нукунону						
7588763	4031091	ar	نوكونونو						
7588764	4031091	ja	ヌクノノ島						
7588765	4031091	ru	Нукунону						
6889837	4031092	link	https://en.wikipedia.org/wiki/Nukumatau						
6889361	4031094	link	https://en.wikipedia.org/wiki/Nukulakia						
1547386	4031103		Matangi Island						
4315691	4031103		Matangi						
6889647	4031103	link	https://en.wikipedia.org/wiki/Matangi%2C_Tokelau						
1547387	4031107		Mulifonua						
4315692	4031107		Fonua Muli						
6889625	4031107	link	https://en.wikipedia.org/wiki/Mulifenua						
1547388	4031108		Fonua Loa Island						
4315693	4031108		Fonua Loa						
8102443	4031108	link	https://en.wikipedia.org/wiki/Fenua_Loa						
1547389	4031109		Fenua Fala						
1547390	4031109		Fonua Fala Island						
4315694	4031109		Fonua Fala						
6889678	4031109	link	https://en.wikipedia.org/wiki/Fenua_Fala						
5424443	4031110		Fakaofu Village						
5424444	4031110		Fale	1					
8434497	4031110	link	https://en.wikipedia.org/wiki/Fale,_Tokelau						
15439342	4031110	wkdt	Q3503088						
1547391	4031111		Fakaafo						
1547392	4031111		Bowditch Island						
1547393	4031111		Fakaofu						
4315695	4031111		Fakaofo Atoll						
5683157	4031111		De Wolf Island						
1547394	4031112		Fakaofu						
2037702	4031112	pl	Fakaofo						
2037703	4031112	de	Fakaofo						
2037704	4031112	en	Fakaofo						
2037705	4031112	bs	Fakaofo						
2037706	4031112	ca	Fakaofo						
2037707	4031112	fi	Fakaofo						
2037708	4031112	hr	Fakaofo						
2037709	4031112	ur	فکاؤفو						
2989002	4031112	link	https://en.wikipedia.org/wiki/Fakaofo						
4315696	4031112		Fakaofo						
13800338	4031112	unlc	TKFKO						
1547395	4031115		Oatafu						
1547396	4031115		Atafu						
1547397	4031115		Duke of York Island						
1547398	4031115		Neu Lauenburg						
1942726	4031115	de	Atafu						
1942727	4031115	en	Atafu						
1942728	4031115	ca	Atafu						
1942729	4031115	fi	Atafu						
1942730	4031115	gl	Atafu						
1942731	4031115	hr	Atafu						
1942732	4031115	ja	アタフ島						
1942733	4031115	ko	아타푸 섬						
1998576	4031115	es	Atafu						
1998577	4031115	bs	Atafu						
1998578	4031115	ur	اٹافو						
4315697	4031115		Atafu Atoll						
7756429	4031115	no	Atafu						
1547399	4031116		Atáfu Island						
2989003	4031116	link	https://en.wikipedia.org/wiki/Atafu						
4315698	4031116		Atafu						
7135964	4031117	link	https://en.wikipedia.org/wiki/Fale%2C_Tokelau						
5648881	4034890		Polynesia - Tokelau						
5424441	7522181		Nukunonu Village						
8412548	7522181	link	https://en.wikipedia.org/wiki/Nukunonu						
13800339	7522181	unlc	TKNKU						
7636538	7522183	link	https://en.wikipedia.org/wiki/Atafu						
7636539	7522183	en	Atafu		1				
7636540	7522183	en	Atafu Village	1					
13800337	7522183	unlc	TKAFU						
15760339	7603121	wkdt	Q35387761						
15098935	7603122	wkdt	Q35387775						
7180117	7603129	link	https://en.wikipedia.org/wiki/Motuhaga						
8690351	7603155	link	https://en.wikipedia.org/wiki/Luana_Liki_Hotel						
15698964	7603264	wkdt	Q35304092						
15753024	7603265	wkdt	Q35304078						
5651743	7603275		Sydney Islet						
5672251	7606385		Te Loto Fakahao						
15811870	7626196	wkdt	Q35298309						
