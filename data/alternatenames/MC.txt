1627392	2992741	ca	Montecarlo						
2299580	2992741	is	Monte Karló						
2426227	2992741	ru	Монте-Карло						
2698079	2992741	es	Montecarlo	1	1				
2964466	2992741	link	https://en.wikipedia.org/wiki/Monte_Carlo						
3054777	2992741	link	https://ru.wikipedia.org/wiki/%D0%9C%D0%BE%D0%BD%D1%82%D0%B5-%D0%9A%D0%B0%D1%80%D0%BB%D0%BE						
5810311	2992741	en	Monte Carlo	1					
7485304	2992741	iata	XMM						
13786976	2992741	unlc	MCMCM						
1284460	2993457	fr	Principauté de Monaco						
1561755	2993457	am	ሞናኮ	1					
1561756	2993457	ar	موناكو	1					
1561757	2993457	bg	Монако	1					
1561758	2993457	ca	Mònaco	1					
1561759	2993457	cs	Monako	1					
1561760	2993457	cy	Monaco	1					
1561761	2993457	da	Monaco	1					
1561762	2993457	de	Monaco	1					
1561763	2993457	el	Μονακό	1					
1561764	2993457	en	Monaco	1	1				
1561765	2993457	eo	Monako	1					
1561766	2993457	es	Mónaco	1	1				
1561767	2993457	et	Monaco	1					
1561768	2993457	eu	Monako	1					
1561769	2993457	fa	موناکو	1					
1561770	2993457	fi	Monaco	1					
1561771	2993457	fo	Monako	1					
1561772	2993457	fr	Monaco	1					
1561773	2993457	ga	Monacó	1					
1561774	2993457	he	מונקו	1					
1561775	2993457	hi	मोनाको	1					
1561776	2993457	hr	Monako	1					
1561777	2993457	hu	Monaco	1					
1561778	2993457	hy	Մոնակո	1					
1561779	2993457	id	Monaco						
1561780	2993457	is	Mónakó	1					
1561781	2993457	it	Monaco	1					
1561782	2993457	ja	モナコ	1					
1561783	2993457	ka	მონაკო	1					
1561784	2993457	km	ម៉ូណាកូ	1					
1561785	2993457	ko	모나코	1					
1561786	2993457	lo	ໂມນາໂຄ	1					
1561787	2993457	lt	Monakas	1					
1561788	2993457	lv	Monako	1					
1561789	2993457	mk	Монако	1					
1561790	2993457	ms	Monaco	1					
1561791	2993457	mt	Monaco	1					
1561792	2993457	nb	Monaco	1					
1561793	2993457	nl	Monaco	1					
1561794	2993457	nn	Monaco						
1561795	2993457	pl	Monako	1					
1561796	2993457	pt	Mônaco	1					
1561797	2993457	ro	Monaco	1					
1561798	2993457	ru	Монако	1					
1561799	2993457	sk	Monako	1					
1561800	2993457	sl	Monako	1					
1561801	2993457	so	Moonako	1					
1561802	2993457	sq	Monako	1					
1561803	2993457	sr	Монако	1					
1561804	2993457	sv	Monaco	1					
1561805	2993457	sw	Monaco	1					
1561806	2993457	ta	மொனாக்கோ	1					
1561807	2993457	th	โมนาโก	1					
1561808	2993457	tr	Monako	1					
1561809	2993457	uk	Монако	1					
1561810	2993457	vi	Monaco	1					
1561811	2993457	zh	摩纳哥	1					
1618654	2993457	af	Monaco	1					
1618655	2993457	an	Monaco						
1618656	2993457	ast	Mónaco						
1618657	2993457	bs	Monako	1					
1618658	2993457	fy	Monako						
1618659	2993457	gl	Mónaco - Monaco						
1618660	2993457	ia	Monaco	1					
1618661	2993457	id	Monako	1					
1618662	2993457	io	Monako						
1618663	2993457	it	Principato di Monaco						
1618664	2993457	kw	Monako						
1618665	2993457	la	Monoecus						
1618666	2993457	lad	Monako						
1618667	2993457	li	Monaco						
1618668	2993457	na	Monaco						
1618669	2993457	nds	Monaco						
1618670	2993457	no	Monaco	1					
1618671	2993457	oc	Monegue						
1618672	2993457	pt	Mónaco	1	1				
1618673	2993457	th	ประเทศโมนาโก						
1618674	2993457	tl	Monaco						
1618675	2993457	ur	مناکو						
1627394	2993457	ca	Principat de Mònaco	1					
1894066	2993457	arc	ܡܘܢܟܘ						
1894067	2993457	br	Monaco	1					
1894068	2993457	ilo	Monaco						
1894069	2993457	lb	Monaco	1					
1894070	2993457	ne	मोनाको	1					
1894071	2993457	oc	Mónegue	1					
1894072	2993457	pam	Monaco						
1894073	2993457	pms	Mònaco						
1894074	2993457	qu	Munaku						
1894075	2993457	tg	Монако	1					
1894076	2993457	ug	موناكو	1					
1894077	2993457	vo	Monakän						
1976118	2993457	frp	Monacô						
1976119	2993457	hi	मोनैको						
1976120	2993457	hsb	Monaco						
1976121	2993457	lij	Principâ de Monego						
1976122	2993457	hbs	Monako						
1976123	2993457	ta	மொனாகோ						
1976124	2993457	ur	موناکو	1					
1976125	2993457	vec	Prinzsipato de Mònaco						
1976126	2993457	war	Monaco						
2421442	2993457	be	Манака	1					
2421443	2993457	bn	মোনাকো	1					
2421444	2993457	bo	མོ་ན་ཀོ།	1					
2421445	2993457	ml	മൊണാക്കോ	1					
2421446	2993457	se	Monaco	1					
2698075	2993457	es	Principado de Mónaco	1					
2920001	2993457	link	https://en.wikipedia.org/wiki/Monaco						
3054780	2993457	link	https://ru.wikipedia.org/wiki/%D0%9C%D0%BE%D0%BD%D0%B0%D0%BA%D0%BE						
6910495	2993457	pt	Principado do Mónaco						
7088558	2993457	ak	Mɔnako	1					
7088559	2993457	az	Monako	1					
7088560	2993457	bm	Monako	1					
7088561	2993457	ee	Monako nutome	1					
7088562	2993457	ff	Monaakoo	1					
7088563	2993457	gl	Mónaco	1					
7088564	2993457	gu	મોનાકો	1					
7088565	2993457	ha	Monako	1					
7088566	2993457	ki	Monako	1					
7088567	2993457	kl	Monaco	1					
7088568	2993457	kn	ಮೊನಾಕೊ	1					
7088569	2993457	ku	Monako	1					
7088570	2993457	lg	Monako	1					
7088571	2993457	ln	Monako	1					
7088572	2993457	lu	Monaku	1					
7088573	2993457	mg	Mônakô	1					
7088574	2993457	mr	मोनॅको	1					
7088575	2993457	my	မိုနာကို	1					
7088576	2993457	nd	Monakho	1					
7088577	2993457	or	ମୋନାକୋ	1					
7088578	2993457	rm	Monaco	1					
7088579	2993457	rn	Monako	1					
7088580	2993457	sg	Monaköo	1					
7088581	2993457	si	මොනාකෝව	1					
7088582	2993457	sn	Monaco	1					
7088583	2993457	te	మొనాకో	1					
7088584	2993457	ti	ሞናኮ	1					
7088585	2993457	to	Monako	1					
7088586	2993457	yo	Monako	1					
7088587	2993457	zu	i-Monaco	1					
12186928	2993457	post	98000						
16766172	2993457	en	Principality of Monaco	1					
16925790	2993457	zh-Hant	摩納哥	1					
16929546	2993457	as	মোনাকো	1					
16929547	2993457	ce	Монако	1					
16929548	2993457	dz	མོ་ན་ཀོ	1					
16929549	2993457	fy	Monaco	1					
16929550	2993457	gd	Monaco	1					
16929551	2993457	ig	Monaco	1					
16929552	2993457	jv	Monako	1					
16929553	2993457	kk	Монако	1					
16929554	2993457	ks	مونیکو	1					
16929555	2993457	ky	Монако	1					
16929556	2993457	mn	Монако	1					
16929557	2993457	pa	ਮੋਨਾਕੋ	1					
16929558	2993457	ps	موناکو	1					
16929559	2993457	qu	Mónaco	1					
16929560	2993457	sd	موناڪو	1					
16929561	2993457	tk	Monako	1					
16929562	2993457	tt	Монако	1					
16929563	2993457	uz	Monako	1					
16929564	2993457	wo	Monako	1					
16929565	2993457	yi	מאנאַקא	1					
1284462	2993458		Monaco-Ville						
1565759	2993458	eo	Monako						
1565760	2993458	es	Mónaco						
1627386	2993458	ca	Mònaco						
1649352	2993458	de	Monaco						
1649382	2993458	en	Monaco						
1649414	2993458	fr	Monaco						
1649452	2993458	it	Monaco						
1649514	2993458	da	Monaco						
1649645	2993458	el	Μονακό						
1649776	2993458	fi	Monaco						
1649813	2993458	pt	Mónaco						
1649848	2993458	sv	Monaco						
1969602	2993458	is	Mónakó						
2426278	2993458	ru	Монако						
2964533	2993458	link	https://en.wikipedia.org/wiki/Monaco						
4278921	2993458		Monaco						
7486639	2993458	iata	MCM						
8545631	2993458	link	https://fr.wikipedia.org/wiki/Monaco						
8545632	2993458	link	https://es.wikipedia.org/wiki/Monaco						
8545633	2993458	link	https://de.wikipedia.org/wiki/Monaco						
8545634	2993458	link	https://no.wikipedia.org/wiki/Monaco						
13786977	2993458	unlc	MCMON						
13895947	2993458	ko	모나코	1					
1288876	3009937		Condamine						
1627391	3009937	ca	Condamine						
2426276	3009937	ru	Ла-Кондамин						
2965527	3009937	link	https://en.wikipedia.org/wiki/La_Condamine						
4286205	3009937		La Condamine						
1627390	3017814	ca	Fontvieille						
2426277	3017814	ru	Фонвьей						
2966194	3017814	link	https://en.wikipedia.org/wiki/Fontvieille%2C_Monaco						
4290380	3017814		Fontvieille						
11460118	3017814	an	Fòntvielha						
11460119	3017814	ca	Fòntvielha						
11460120	3017814	hy	Ֆոնվյեյ						
11460121	3017814	ko	퐁비에유						
11460122	3017814	lt	Fonvjėjus						
11460123	3017814	oc	Fòntvielha						
11460124	3017814	uk	Фонтвілль						
11460125	3017814	zh	芳特维耶						
1291341	3018192		Pointe Focinana						
1627397	3018192	ca	Cap de Fucinane						
4290600	3018192		Pointe Focinane						
1376028	3225753		Point Saint-Martin						
1627401	3225753	ca	Punta de Saint Martin						
4743158	3225753		Pointe Saint-Martin						
1627400	3225756	ca	Punta de la Poudrière						
4743159	3225756		Pointe de la Poudrière						
1627399	3225758	ca	Punta de Saint Antoine						
4743160	3225758		Pointe Saint-Antoine						
1376029	3225759		Port de Monaco						
1376030	3225759		Port de la Condamine						
1627387	3225759	ca	Port de Mònaco						
4743162	3225759		Port Hercule						
16710502	3225759	unlc	MCMON						
1381062	3319157		Stade Louis II						
1938623	3319157	es	Estadio Luis II						
1938624	3319157	fr	Stade Louis-II						
1938625	3319157	de	Stade Louis II						
1938626	3319157	en	Stade Louis II						
1938627	3319157	fi	Stade Louis II						
1938628	3319157	ja	スタッド・ルイ・ドゥ						
1938629	3319157	nl	Stade Louis II						
1938630	3319157	sv	Stade Louis II						
2015103	3319157	it	Stadio Louis II						
2983809	3319157	link	https://en.wikipedia.org/wiki/Stade_Louis_II						
4801346	3319157		Stade Louis Deux						
15636055	3319160	wkdt	Q27557569						
16028269	3319160		Ciappaira						
1627396	3319162	ca	Platje de Larvotto						
4801350	3319162		Plage du Larvotto						
15905738	3319162	wkdt	Q27557568						
1627398	3319163	ca	Àrea de Larvotto						
4801351	3319163		Larvotto						
7202922	3319163	link	https://en.wikipedia.org/wiki/Larvotto						
1381063	3319170		Réserve Sous-marine du Larvotto						
1627388	3319170	ca	Reserva submarina de Larvotto						
4801358	3319170		Réserve Sous-marine de Monaco						
1627389	3319174	ca	Jardins japonesos						
4801361	3319174		Jardin Japonais						
1627393	3319177	ca	Monegue						
4801374	3319177		Moneghetti	1					
6774234	3319177		Les Moneghetti						
7187384	3319177	link	https://en.wikipedia.org/wiki/Les_Moneghetti						
9529814	3319177	ru	Монегетти	1					
1627395	3319178	ca	Comú de Mònaco						
2698076	3319178	es	Comuna de Mónaco	1	1				
11713250	3319178	fr	Monaco						
16202977	3319178	link	https://en.wikipedia.org/wiki/Municipality_of_Monaco						
16708792	3319178	en	Municipality of Monaco						
5901563	6954047	link	https://en.wikipedia.org/wiki/Grimaldi_Forum						
7236407	8029384		Beausoleil						
7236408	8029385		Les Révoires						
6871634	8029387		Cap d’Ail						
6871635	8029387		Pointe des Douaniers						
7625816	8298434	post	98000						
7625817	8298435	post	98000						
7625818	8298436	post	98000						
7626148	8298437	post	98000						
7626149	8298438	post	98000						
7625819	8298439	post	98020						
7625820	8298440	post	98001						
8066516	8504785	link	https://en.wikipedia.org/wiki/Monaco-Ville						
8066517	8504790	link	https://en.wikipedia.org/wiki/Saint_Michel,_Monaco						
8066518	8504791	link	https://en.wikipedia.org/wiki/La_Colle,_Monaco						
11525075	8504793	an	La Condamina						
11525076	8504793	be	Ла-Кандамін						
11525077	8504793	ca	La Condamina						
11525078	8504793	eo	Kondamino						
11525079	8504793	hy	Լա Կոնդամին						
11525080	8504793	ko	라콩다민						
11525081	8504793	la	Condamina						
11525082	8504793	lt	La Kondaminas						
11525083	8504793	oc	La Condamina						
11525084	8504793	uk	Ла-Кондамін						
11525085	8504793	ur	لا کونڈامینی						
11525086	8504793	ru	Ла-Кондамин						
11525087	8504793	zh	拉康达明						
7130379	8504794	link	https://en.wikipedia.org/wiki/Les_R%C3%A9voires						
8066519	8504794	link	https://en.wikipedia.org/wiki/Fontvieille,_Monaco						
8145648	8520871	icao	LNMC						
8145649	8520871	link	https://en.wikipedia.org/wiki/Monaco_Heliport						
8150598	8520871	iata	MCM						
8150599	8520871	en	Monaco Fontvieille Heliport						
8150600	8520871	fr	Héliport de Monaco Fontvieille						
13895993	8520871	unlc	MCMON						
16146244	10278967	link	https://en.wikipedia.org/wiki/Monaco_Top_Cars_Collection						
16076970	10278971	fr	Palais Princier		1				
16076971	10278971	it	Palazzo dei Principi di Monaco	1					
16076972	10278971	fr	Palais des Princes de Monaco						
16076974	10278971	fr	Palais de Monaco	1					
16076975	10278971	it	Palazzo Grimaldi						
16076976	10278971	wkdt	Q1164788						
16144312	10278971	link	https://en.wikipedia.org/wiki/Prince%27s_Palace_of_Monaco						
16198562	10278972	link	https://en.wikipedia.org/wiki/Oceanographic_Museum						
15508492	10278975	wkdt	Q1955711						
16135813	10278975	link	https://en.wikipedia.org/wiki/Chapelle_de_la_Visitation%2C_Monaco-Ville						
11285534	10278991	en	Monte Carlo Casino						
11285535	10278991	link	https://en.wikipedia.org/wiki/Monte_Carlo_Casino						
16076965	11983649	fr	Grand Théâtre de Monte Carlo	1					
16076966	11983649	en	Monte-Carlo Opera House	1					
16076967	11983649	it	Opera di Monte Carlo	1					
16076968	11983649	wkdt	Q1577048						
16076969	11983649	fr	Salle Garnier				1		
16076984	11983662	en	Cathedral of Our Lady Immaculate	1					
16076985	11983662	en	Saint Nicholas Cathedral				1		
16076986	11983662	en	Monaco Cathedral						
16076987	11983662	fr	Cathédrale de Monaco						
16076988	11983662	fr	Cathédrale de Notre-Dame-Immaculée	1					
16076989	11983662	fr	Église Saint-Nicolas				1		
16076990	11983662	wkdt	Q667450						
16076991	11983662	it	Cattedrale dell'Immacolata Concezione	1					
16775730	11983662	en	St. Nicholas Cathedral				1		
16076993	11983665	fr	Église Saint-Charles	1					
16076994	11983665	en	Saint Charles Church	1					
16076995	11983665	it	Chiesa di San Carlo	1					
16775729	11983665	en	St. Charles Church						
16076996	11983667	fr	Théâtre Princesse Grace	1					
16076997	11983667	en	Princess Grace Theatre	1					
16076998	11983667	it	Teatro Principessa Grace	1					
16076999	11983667	wkdt	Q555184						
16363370	12060734	uicn	87756403						
