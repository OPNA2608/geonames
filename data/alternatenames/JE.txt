4332845	3042075	en	Violet Channel						
1298371	3042076		Banc De Violet						
4332846	3042076	en	Violet Bank						
2360414	3042077		Le Manoir de Vinchelez de Haut						
4332847	3042077		Vinchelez de Haut						
2360415	3042078		Le Manoir de Vinchelez de Bas						
4332848	3042078		Vinchelez de Bas						
1298372	3042079		Ville ès Quenvais						
1298373	3042079		Ville des Quenvais						
2360416	3042079		La Ville des Quennevais						
4332849	3042079	en	Ville des Quennevais						
2360417	3042080		Vicard						
4332850	3042080	en	Vicard Point						
4332851	3042081	en	Verclut Point						
1298374	3042082		Valley des Vaux						
1298375	3042082		Val des Vaux						
4332852	3042082	en	Vallée des Vaux						
1298376	3042083		Rozel Tower						
2360418	3042083		La Tour de Rozel						
4332853	3042083		Tour de Rozel						
2360419	3042084		Sorel						
4332854	3042084	en	Sorel Point						
4332855	3042085	en	Six Rues						
1298377	3042086		Saint Peter’s						
1298378	3042086		Saint Peter Church						
1298379	3042086		Saint Peter						
4332856	3042086	en	Saint Peter’s Church						
16766736	3042086	en	St. Peter’s Church						
1298380	3042087		La Mare au Seigneur						
2360420	3042087		St. Ouen’s Pond						
4332857	3042087	en	Saint Ouen’s Pond						
16766737	3042087	en	St. Ouen’s Pond						
1298381	3042088		Saint Ouen Bay						
2360421	3042088		La Baie de St. Ouën						
2360422	3042088		St. Ouën’s Bay						
4332858	3042088	en	Saint Ouen’s Bay						
13773279	3042088	unlc	GBUEN						
16766738	3042088	en	St. Ouen’s Bay						
1298382	3042089		Saint Jean						
1298383	3042089		Saint John’s						
1298384	3042089	en	Saint John		1				
2360423	3042089		St. John’s Church						
4332859	3042089	en	Saint John’s Church						
7166478	3042089	link	https://en.wikipedia.org/wiki/Saint_John%2C_Jersey						
13784030	3042089	unlc	JESJN						
16766739	3042089	en	St. John’s Church						
16775721	3042089	en	St. John						
4332860	3042090	en	Saint John’s Bay						
16766740	3042090	en	St. John’s Bay						
2426719	3042091	ru	Сент-Хельер						
2968613	3042091	link	https://en.wikipedia.org/wiki/Saint_Helier						
3055058	3042091	link	https://ru.wikipedia.org/wiki/%D0%A1%D0%B5%D0%BD%D1%82-%D0%A5%D0%B5%D0%BB%D1%8C%D0%B5%D1%80						
4332861	3042091	en	Saint Helier	1					
7625085	3042091	en	St Helier						
7625086	3042091	en	St. Helier						
8065510	3042091	iata	JER						
9710026	3042091	de	Saint Helier	1					
9710027	3042091	fr	Saint-Hélier	1					
13773173	3042091	unlc	GBTNR						
13784033	3042091	unlc	JESTH						
13895964	3042091	ko	세인트헬리어						
16894344	3042091	eo	Sankt' Heliero						
1298385	3042092		Saint Clement Bay						
4332862	3042092	en	Saint Clements Bay						
16766741	3042092	en	St. Clements Bay						
1298386	3042093		Saint Catherines Bay						
4332863	3042093	en	Saint Catherine Bay						
16766742	3042093	en	St. Catherine Bay						
2360424	3042094		Saint Brelade Bay						
2360425	3042094		St. Brelade’s Bay						
2360426	3042094		La Baie de Saint Brélade						
4332864	3042094		Saint Brelade’s Bay						
1298388	3042095		Saint Aubyn Bay						
1298389	3042095		Saint Aubin Bay						
4332865	3042095	en	Saint Aubins Bay						
16766743	3042095	en	St. Aubins Bay						
1298390	3042096		Saint Aubyn						
4332866	3042096		Saint Aubin						
7526909	3042096	link	https://en.wikipedia.org/wiki/Saint_Aubin%2C_Jersey						
13784028	3042096	unlc	JESAB						
16432948	3042096	de	St Aubin						
2360427	3042097		Le Havre de Rozel						
4332867	3042097		Rozel Bay						
1298391	3042098		Rozel Seigneurie						
4332868	3042098	en	Rozel						
1298392	3042099		Rousse						
4332869	3042099	en	Roussé Rock						
2360428	3042100		Rouge Nez						
2360429	3042100		La Tête d’Âne						
4332870	3042100		Rouge Nez Point						
4332871	3042101	en	Rouge Nez						
2360430	3042102		Ronez						
4332872	3042102	en	Ronez Point						
1298394	3042103		The Quennevais						
2360431	3042103	en	The Quenvais						
4332873	3042103		Les Quennevais						
2360432	3042104		Le Portelet						
4332874	3042104	en	Portelet Bay						
1298395	3042105		Pleinmont Point						
2360433	3042105		La Tête de Plémont						
4332875	3042105	en	Plémont Point						
1298396	3042106		Plat Rocque Point						
1298397	3042106		Platte Roque Point						
4332876	3042106	en	Plat Roque Point						
2360434	3042107		Le Petit Port						
4332877	3042107	en	Petit Port						
2360435	3042108		Le Petit Port						
4332878	3042108	en	Petit Port						
2360436	3042109		L’Île Perchie						
4332879	3042109		Île Percée						
4332880	3042111	en	Millbrook						
4332881	3042113	en	Longueville						
1298398	3042114		L’Etac						
4332882	3042114		L’Etacq						
4332883	3042117	en	Les Landes						
2360437	3042118		Les Caînes						
4332884	3042118		Les Kaines						
1298399	3042119		Ecrehos						
2968614	3042119	link	https://en.wikipedia.org/wiki/%C3%89cr%C3%A9hous						
4332885	3042119	en	Les Écrehou						
4332886	3042120	en	Le Nez Point						
15558126	3042120	wkdt	Q27118253						
15333429	3042121	wkdt	Q27118252						
1298400	3042122		La Fret Point						
2360438	3042122		Le Fret						
4332887	3042122	en	Point Le Fret						
1298401	3042123		Le Croq						
1298402	3042123		Le Croc						
1298403	3042123		Point Le Croo						
1298404	3042124		Paternosters						
1298405	3042124		Pierre de Lecq						
1298406	3042124		Pierres Des Lecq						
4332888	3042124		Pierres de Lecq						
7182100	3042124	link	https://en.wikipedia.org/wiki/Pierres_de_Lecq						
4332889	3042125	en	Le Couperon						
15795842	3042125	wkdt	Q27118249						
15504007	3042126	wkdt	Q27118248						
15969328	3042126		La Thiebaut						
1298407	3042127		La Roque Point						
4332890	3042127	en	La Rocque Point						
1298408	3042129		Point la Moye						
4332891	3042129	en	Point La Moye						
1298409	3042130		Green Island						
7544553	3042130	link	https://en.wikipedia.org/wiki/La_Motte%2C_Jersey						
15437782	3042130	wkdt	Q2670037						
1298410	3042131		La Hougue Boëtte						
4332892	3042131	en	La Hougue Boëte						
1298411	3042132		Hougue Bie						
1298412	3042132		Prince’s Tower						
4332893	3042132	en	La Hougue Bie						
7155220	3042132	link	https://en.wikipedia.org/wiki/La_Hougue_Bie						
4332894	3042133	en	La Hougue						
4332895	3042134	en	La Haule						
1298413	3042135		La Crete						
1298414	3042135		Crete Point						
4332896	3042135	en	La Crête Point						
4332897	3042136	en	La Crête Point						
1298415	3042137		Coupe Point						
1298416	3042137		La Coupe						
4332898	3042137		La Coupe Point						
15227213	3042137	wkdt	Q27118242						
15422213	3042138	wkdt	Q27118241						
15969329	3042138		La Cotte Point						
4332899	3042139	en	La Corbière						
7176202	3042139	link	https://en.wikipedia.org/wiki/La_Corbi%C3%A8re						
4332900	3042140	en	La Conchière						
2360439	3042141		La Colonbine						
4332901	3042141		La Colombière						
2256623	3042142	fr	Jersey	1					
2256626	3042142	fr	Bailliage de Jersey						
2421918	3042142	ar	جيرسي	1					
2421919	3042142	bg	Джърси	1					
2421920	3042142	ca	Jersey	1					
2421921	3042142	cs	Jersey	1					
2421922	3042142	da	Jersey	1					
2421923	3042142	de	Jersey	1					
2421924	3042142	el	Τζέρζι	1					
2421925	3042142	en	Jersey	1					
2421926	3042142	es	Jersey	1					
2421927	3042142	et	Jersey	1					
2421928	3042142	eu	Jersey	1					
2421929	3042142	fi	Jersey	1					
2421930	3042142	ga	Geirsí	1					
2421931	3042142	gl	Jersey	1					
2421932	3042142	he	ג׳רזי	1					
2421933	3042142	hi	जर्सी	1					
2421934	3042142	hr	Jersey	1					
2421935	3042142	hu	Jersey	1					
2421936	3042142	id	Jersey	1					
2421937	3042142	is	Jersey	1					
2421938	3042142	it	Jersey	1					
2421939	3042142	ja	ジャージー	1					
2421940	3042142	ka	ჯერსი	1					
2421941	3042142	ko	저지	1					
2421942	3042142	lt	Džersis	1					
2421943	3042142	lv	Džērsija	1					
2421944	3042142	nb	Jersey	1					
2421945	3042142	nl	Jersey	1					
2421946	3042142	nn	Jersey	1					
2421947	3042142	pl	Jersey	1					
2421948	3042142	pt	Jersey	1					
2421949	3042142	ro	Jersey	1					
2421950	3042142	ru	Джерси	1					
2421951	3042142	se	Jersey	1					
2421952	3042142	sk	Jersey	1					
2421953	3042142	sl	Jersey	1					
2421954	3042142	sr	Џерзи	1					
2421955	3042142	sv	Jersey	1					
2421956	3042142	th	เจอร์ซีย์	1					
2421957	3042142	tr	Jersey	1					
2421958	3042142	uk	Джерсі	1					
2421959	3042142	ur	جرسی	1					
2421960	3042142	vi	Jersey	1					
2421961	3042142	zh	泽西岛	1					
5424035	3042142	link	https://en.wikipedia.org/wiki/Jersey						
7087716	3042142	af	Jersey	1					
7087717	3042142	am	ጀርሲ	1					
7087718	3042142	az	Cersi	1					
7087719	3042142	bn	জার্সি	1					
7087720	3042142	br	Jerzenez	1					
7087721	3042142	bs	Jersey	1					
7087722	3042142	ee	Dzɛse nutome	1					
7087723	3042142	fa	جرزی	1					
7087724	3042142	fo	Jersey	1					
7087725	3042142	gu	જર્સી	1					
7087726	3042142	kl	Jersey	1					
7087727	3042142	kn	ಜೆರ್ಸಿ	1					
7087728	3042142	mk	Џерси	1					
7087729	3042142	ml	ജേഴ്സി	1					
7087730	3042142	mr	जर्सी	1					
7087731	3042142	ms	Jersey	1					
7087732	3042142	my	ဂျာစီ	1					
7087733	3042142	ne	जर्सी	1					
7087734	3042142	or	ଜର୍ସି	1					
7087735	3042142	rm	Jersey	1					
7087736	3042142	sw	Jersey	1					
7087737	3042142	ta	ஜெர்சி	1					
7087738	3042142	te	జెర్సీ	1					
7087739	3042142	to	Selusī	1					
7087740	3042142	zu	i-Jersey	1					
16925763	3042142	zh-Hant	澤西島	1					
16928990	3042142	as	জাৰ্চি	1					
16928991	3042142	be	Джэрсі	1					
16928992	3042142	ce	Джерси	1					
16928993	3042142	cy	Jersey	1					
16928994	3042142	dz	ཇེར་སི	1					
16928995	3042142	fy	Jersey	1					
16928996	3042142	gd	Deàrsaidh	1					
16928997	3042142	ha	Kasar Jersey	1					
16928998	3042142	hy	Ջերսի	1					
16928999	3042142	ia	Jersey	1					
16929000	3042142	ig	Jersey	1					
16929001	3042142	jv	Jersey	1					
16929002	3042142	kk	Джерси	1					
16929003	3042142	km	ជើស៊ី	1					
16929004	3042142	ks	جٔرسی	1					
16929005	3042142	ky	Жерси	1					
16929006	3042142	lb	Jersey	1					
16929007	3042142	ln	Jelezy	1					
16929008	3042142	lo	ເຈີຊີ	1					
16929009	3042142	mn	Жерси	1					
16929010	3042142	mt	Jersey	1					
16929011	3042142	no	Jersey	1					
16929012	3042142	pa	ਜਰਸੀ	1					
16929013	3042142	ps	جرسی	1					
16929014	3042142	qu	Jersey	1					
16929015	3042142	sd	جرسي	1					
16929016	3042142	si	ජර්සි	1					
16929017	3042142	so	Jaarsey	1					
16929018	3042142	sq	Xhersej	1					
16929019	3042142	tg	Ҷерси	1					
16929020	3042142	ti	ጀርሲ	1					
16929021	3042142	tk	Jersi	1					
16929022	3042142	tt	Джерси	1					
16929023	3042142	ug	جېرسېي	1					
16929024	3042142	uz	Jersi	1					
16929025	3042142	wo	Serse	1					
16929026	3042142	yi	דזשערזי	1					
16929027	3042142	yo	Jersey	1					
1298418	3042143		Isle of Jersey						
1895551	3042143	en	Jersey						
1895552	3042143	de	Jersey						
1895553	3042143	fr	Jersey						
1895554	3042143	pl	Jersey						
1895555	3042143	es	Jersey						
1895556	3042143	af	Jersey						
1895557	3042143	ar	جزيرة جيرزي						
1895558	3042143	ast	Jersey						
1895559	3042143	bs	Jersey						
1895560	3042143	ca	Jersey						
1895561	3042143	cs	Jersey						
1895562	3042143	da	Jersey						
1895563	3042143	el	Τζέρσεϋ						
1895564	3042143	eo	Jersey						
1895565	3042143	fi	Jersey						
1895566	3042143	frp	Jèrseyi						
1895567	3042143	he	ג'רזי						
1895568	3042143	hr	Jersey						
1895569	3042143	hu	Jersey						
1895570	3042143	id	Jersey						
1895571	3042143	io	Jersey						
1895572	3042143	is	Jersey						
1895573	3042143	it	Jersey						
1895574	3042143	ja	ジャージー島						
1895575	3042143	ka	ჯერსი						
1895576	3042143	ko	저지 섬						
1895577	3042143	kw	Jersi						
1895578	3042143	li	Jersey						
1895579	3042143	lv	Džersija						
1895580	3042143	nl	Jersey						
1895581	3042143	nn	Jersey						
1895582	3042143	no	Jersey						
1895583	3042143	nrm	Jèrri						
1895584	3042143	oc	Gersei						
1895585	3042143	pt	Jersey						
1895586	3042143	ro	Insula Jersey						
1895587	3042143	ru	Джерси						
1895588	3042143	hbs	Jersey						
1895589	3042143	sk	Jersey						
1895590	3042143	sl	Jersey						
1895591	3042143	sq	Jersey						
1895592	3042143	sr	Џерси						
1895593	3042143	sv	Jersey						
1895594	3042143	tr	Jersey						
1895595	3042143	uk	Джерсі						
1895596	3042143	zh	澤西島						
1981433	3042143	la	Caesarea Maritima						
1981434	3042143	lij	Jersey						
1981435	3042143	oc	Jersey						
1981436	3042143	war	Jersey						
3055059	3042143	link	https://ru.wikipedia.org/wiki/%D0%94%D0%B6%D0%B5%D1%80%D1%81%D0%B8						
10641865	3042143	iata	JER						
13895982	3042143	unlc	JEJER						
16317387	3042143	link	https://en.wikipedia.org/wiki/Jersey						
1298419	3042144		Hermatage						
1298420	3042144		Hermitage						
2360440	3042144		Le Hermitage de Saint Hélier						
2360441	3042144		L’Oratoire						
4332902	3042144	en	Hermitage Rock						
2360442	3042145		L’Île au Guerdain						
2360443	3042145		Isle au Guerdain						
4332903	3042145		Île au Guerdain						
1298421	3042146		Royal Bay of Grouville						
4332904	3042146	en	Grouville Bay						
2360444	3042147		Gros Nez						
4332905	3042147	en	Grosnez Point						
2360445	3042148		La Grève de Lecq						
4332906	3042148		Grève de Lecq						
1298422	3042149		Greve au L’anchon						
2360446	3042149		La Grève au Lanchon						
2360447	3042149		Plémont Beach						
4332907	3042149		Grève au Lançon						
4332908	3042150	en	Grand Chemins						
1298423	3042151		Gouray						
2968615	3042151	link	https://en.wikipedia.org/wiki/Gorey%2C_Jersey						
1298424	3042152		Gifford Bay						
2360448	3042152		Le Havre Giffard						
4332909	3042152	en	Giffard Bay						
2360449	3042153		La Tête de Frémont						
4332910	3042153	en	Fremont Point						
1298425	3042154		Fliquet						
4332911	3042154	en	Fliquet Bay						
2360450	3042155		Le Douët de la Mer						
4332912	3042155		Douet de la Mer						
1298426	3042156		Dirouilles						
4332913	3042156	en	Les Dirouilles						
7519777	3042156	link	https://en.wikipedia.org/wiki/Les_Dirouilles						
1298427	3042157		Point Corbière						
4332914	3042157	en	Corbière Point						
1298428	3042158		Boulay Bay						
2360451	3042158		Le Boulay						
4332915	3042158	en	Bouley Bay						
2360452	3042159		La Saline						
4332916	3042159		Bouilly Port						
2360453	3042160		Bonne Nuit						
4332917	3042160	en	Bonne Nuit Bay						
8671407	3042160	link	https://en.wikipedia.org/wiki/Bonne_Nuit_%28Jersey%29						
16433819	3042162	link	https://en.wikipedia.org/wiki/Belle_Vue,_Bradford						
16437054	3042162		Belle Vue						
2360454	3042163		La Belle Hougue						
4332918	3042163	en	Belle Hougue Point						
15740807	3042163	wkdt	Q27118719						
2360455	3042164		Belcroute						
4332919	3042164	en	Belcroute Bay						
1298429	3042168		Les Aiguillons						
4332920	3042168		Aiguillon						
1298478	3042318		Minque						
1298479	3042318		Miniquers						
1298480	3042318		Miniquiers						
1298481	3042318		Minquiers						
1298482	3042318		Les Minquiers						
2968637	3042318	link	https://en.wikipedia.org/wiki/Minquiers						
4334657	3042318	en	Plateau des Minquiers						
8440357	3042318	en	The Minkies						
8440358	3042318		Les Mîntchièrs						
8440359	3042318	en	The Minquiers						
4765570	3234497	en	West Rock						
4767609	3236962	en	Icho Bank						
4767610	3236964	en	Plateau de la Frouquie						
4767611	3236967	en	La Rousse Platte						
4767612	3236968	en	L’Échiquelez						
4767613	3236971	en	La Longy						
4767614	3236973	en	Gros Étacs						
4767615	3236974	en	La Grande Frouquie						
4767616	3236975	en	La Rousse						
4767617	3236976	en	Jinquet Rock						
4767618	3236979	en	Rouget Rock						
4767619	3236981	en	Margaret Rock						
2364242	3236983		La Demi des Pas						
4767620	3236983		Demie de Pas						
4767621	3236986	en	Little Demie						
4767622	3236987	en	La Conière						
4767623	3236988	en	L’teton						
4767624	3236989	en	Rocque Herbeuse						
4767625	3236990	en	Rocque Larron						
4767627	3236993	en	Havre de Fontaines						
4767628	3237006	en	La Trouee						
4767629	3237008	en	Fort Henry						
4767630	3237010	en	Round Rouget						
4767631	3237011	en	Bailhache						
2364243	3237013		Carré du Cané						
4767632	3237013	en	Flat Rock						
2364244	3237014		La Noire						
4767633	3237014		La Ronde						
2364245	3237016		White Rock						
2364246	3237016		Le Tas d’Pais						
4767635	3237016		Tas de Pois						
2364247	3237017		Les Trais Grunes						
4767637	3237017		Trois Grunes						
2364248	3237018		White Rock						
2364249	3237018		Hinguette						
4767638	3237018	en	Nipple Rock						
2364250	3237019		La Mondine						
4767639	3237019		Mondine						
4767640	3237021	en	Dog’s Nest Rock						
2364251	3237024		Crabière						
4767641	3237024	en	Crabière Rock						
2364252	3237025		Le Havre des Pas						
4767642	3237025		Hâvre des Pas						
1377293	3237027		Point de Pas						
4767644	3237027	en	Point des Pas						
4767646	3237028	en	East La Cloche						
4767647	3237029	en	West La Cloche						
4767648	3237030	en	South Reef						
4767649	3237031	en	East Rock						
4767650	3237032	en	West Rock						
4767651	3237033	en	Moulet Rock						
4767652	3237034	en	Quereme Rock						
4767653	3237035	en	Petite Mangeuse						
4767656	3237036	en	Grande Mangeuse						
4767657	3237037	en	Crapaud of the Mangeuse						
4767658	3237038	en	Small Road						
4767659	3237039	en	Sharp Rock						
4767660	3237040	en	Platte Rock						
2364253	3237041		L’Hitrière						
2364254	3237041		Oyster Rock						
4767661	3237041	en	Oyster Rocks						
4767662	3237042	en	Hermitage Breakwater						
2364255	3237043		Les Hinguettes						
4767667	3237043		Hinguette						
4767668	3237045	en	Fairway Rock						
2364256	3237046		Rouaudière Rock						
4767669	3237046		Ruaudière Rock						
4767670	3237047	en	Diamond Rock						
1377294	3237048		Saint Helier Harbor						
4767671	3237048	en	Saint Helier Harbour						
16766746	3237048	en	St. Helier Harbour						
4767672	3237049	en	Mount Bingham						
4767673	3237050	en	Crapaud of the Castle						
4767674	3237051	en	Southeast Rock						
4767675	3237052	en	Old Harbour						
4767676	3237053	en	French Harbour						
4767677	3237054	en	English Harbour						
4767678	3237055	en	Saint Helier Marina						
8096445	3237055	link	https://en.wikipedia.org/wiki/Saint_Helier_Marina						
16766747	3237055	en	St. Helier Marina						
2364257	3237056		North Quay						
4767679	3237056		New North Quay						
4767680	3237057	en	Albert Pier						
4767681	3237058	en	South Pier						
4767682	3237059	en	Victoria Pier						
4767683	3237060	en	East Pier						
4767684	3237061	en	West Breakwater						
4767687	3237062	en	Les Bûts						
4767688	3237063	en	Crow Rock						
2254302	3237064	en	Elizabeth Castle	1					
2983685	3237064	link	https://en.wikipedia.org/wiki/Elizabeth_Castle						
4767691	3237064	en	Fort Elizabeth						
2364258	3237065		Le Château Elisabeth						
2364259	3237065		Le Château de L’Islet						
4767692	3237065	en	Elizabeth Castle						
4767693	3237066	en	Fort Charles						
2364260	3237067		Le Gros du Château						
4767694	3237067		Gros du Château						
2364261	3237069		La Vrachère						
4767696	3237069		La Vrachière						
4767700	3237071	en	La Collette Yacht Basin						
3075865	3237072	en	St Clement	1					
7176301	3237072	link	https://en.wikipedia.org/wiki/Saint_Clement%2C_Jersey						
8488028	3237072		St Cliément						
9394638	3237072	fr	Saint-Clément						
9394639	3237072	br	Saint Cliément						
9394640	3237072	nrm	Saint Cliément						
11558458	3237072	ur	سینٹ کلیمنٹ، جرسی						
7129872	3237073	link	https://en.wikipedia.org/wiki/Saint_Saviour%2C_Jersey						
8488030	3237073		St Saûveux						
8488031	3237073		St Sauveur						
9069865	3237073	en	St Saviour	1					
9394621	3237073	fr	Saint-Sauveur						
9394622	3237073	br	Saint Saûveux						
9394623	3237073	nrm	Saint Saûveux						
13784032	3237073	unlc	JESSV						
4767762	3237078	en	Danger Rock						
2364285	3237079		Les Grunes ès Dards						
2364286	3237079		Grune aux Dardes						
4767763	3237079		Grunes aux Dardes						
2364287	3237086		Le Roc Aumont						
4767765	3237086	en	Beach Rock						
1377297	3237087		Saint Aubin Castle						
2364288	3237087		Le Vièr Fort						
2364289	3237087		Saint Aubin Fort						
2364290	3237087		St. Aubin’s Fort						
4767766	3237087		Saint Aubin’s Fort						
2364291	3237088		Platte						
2364292	3237088		La Platte						
4767767	3237088		Platte Rock						
2364293	3237099		Grunes Vaudin						
4767772	3237099		Les Grunes Vaudin						
4767773	3237100	en	Southwest Rock						
4767777	3237110	en	Passage Rock						
2364294	3237112		Les Sillettes						
4767783	3237112		Sillette						
2364295	3237116		Le Pignonet						
4767784	3237116		Pignonet						
4767785	3237117	en	Portelet Ledge						
2364296	3237118		Le Bût						
4767787	3237118		Point de Bût						
2364297	3237131		Becquet						
4767788	3237131		Rocquet						
4767789	3237132	en	Fournier Rock						
2364298	3237139		Le Ouaisné						
2364299	3237139		L’Ouaisne						
4767791	3237139		Ouaisné Bay						
1377298	3237141		Point le Grouin						
2364300	3237141		Le Grouin						
4767792	3237141		Point Le Grouin						
2364301	3237191		Le Beau Port						
4767808	3237191		Beau Port						
2364302	3237193		Le Mont Ficquet						
4767810	3237193	en	Mont Fiquet						
2364303	3237194		Les Leaux de Ficquet						
4767812	3237194	en	Fiquet Bay						
2364304	3237200	en	St. Brelade	1					
2364305	3237200		St. Brelade’s						
4767814	3237200		Saint Brelade						
7193061	3237200	link	https://en.wikipedia.org/wiki/Saint_Br%C3%A9lade						
13773078	3237200	unlc	GBTBR						
16766379	3237200	en	Saint Brelade						
2364306	3237201		La Ville ès Nouaux						
4767815	3237201		Ville ès Nouaux						
4767816	3237203	en	Grouville						
7147554	3237203	link	https://en.wikipedia.org/wiki/Grouville						
8081840	3237206	link	https://en.wikipedia.org/wiki/Fort_Regent						
15641870	3237206	wkdt	Q3078026						
2364307	3237212		St. Mary’s						
2364308	3237212		St. Mary						
4767821	3237212	en	Saint Mary						
7141354	3237212	link	https://en.wikipedia.org/wiki/Saint_Mary%2C_Jersey						
8488037	3237212		Sainte Mathie						
9069842	3237212	en	St Mary	1					
9393935	3237212	fr	Sainte-Marie						
16766750	3237212	en	St. Mary						
4767822	3237214	en	Saint Lawrence						
7191995	3237214	link	https://en.wikipedia.org/wiki/Saint_Lawrence%2C_Jersey						
8488032	3237214		St Louothains						
9069825	3237214	en	St Lawrence	1					
9394632	3237214	fr	Saint-Laurent						
9394633	3237214	br	Saint Louothains						
9394634	3237214	nrm	Saint Louothains						
16766751	3237214	en	St. Lawrence						
2364309	3237221		Saint Peter’s						
2364310	3237221		St. Peter						
4767824	3237221	en	Saint Peter						
7189290	3237221	link	https://en.wikipedia.org/wiki/Saint_Peter%2C_Jersey						
8488038	3237221		St Pièrre						
9069850	3237221	en	St Peter	1					
9394624	3237221	fr	Saint-Pierre						
9394625	3237221	br	Saint Pièrre						
9394626	3237221	nrm	Saint Pièrre						
11579273	3237221	ur	سینٹ پیٹر، جرسی						
13784031	3237221	unlc	JESPT						
16766713	3237221	en	St. Peter						
2364311	3237229	en	St. Ouen						
4767826	3237229	en	Saint Ouen	1					
7159868	3237229	link	https://en.wikipedia.org/wiki/Saint_Ouen%2C_Jersey						
8488040	3237229		Saint Ouën						
8666488	3237229	en	St. Ouen's			1			
9069843	3237229	en	St Ouen		1				
9394663	3237229	fr	Saint-Ouen						
11634446	3237229	ur	سینٹ وں ، جرسی						
2364313	3237233		Blanches Banques						
4767827	3237233		Les Blanches Banques						
13784027	3237235	unlc	JELAF						
13828833	3237235		La Fosse						
13857788	3237235	en	La Fosse						
2364314	3237236		La Grosse Tête						
4767828	3237236		Grosse Tête						
1377301	3237242		Jument						
2364315	3237242		La Jument						
4767833	3237242	en	Jument Rock						
2364316	3237243		Noirmontaise						
4767834	3237243	en	Noirmontaise Reef						
4767836	3237244	en	Les Boîteaux						
4767837	3237245	en	Green Rock						
4767838	3237247	en	La Frouquie						
4767839	3237248	en	Great Bank						
4767840	3237249	en	Flat Rock						
4767841	3237250	en	Sharp Rock						
4767847	3237251	en	Rigdon Bank						
1377302	3237252		Petite Etaquerel						
2364317	3237252		Le Petit Étacquerel						
4767849	3237252	en	Petit Étaquerel						
1377303	3237253		Grande Etaquerel						
2364318	3237253		Le Grande Étacquerel						
4767850	3237253	en	Grand Étaquerel						
4767851	3237254	en	Swashway Channel						
4767852	3237255	en	Mouillière Rock						
4767853	3237256	en	North East Rock						
2364387	3237290		La Ville au Bas						
4767970	3237290		Ville au Bas						
4767971	3237295	en	North West Head						
4767972	3237300	en	Great Rock						
4767973	3237301	en	Sharp Rock						
4767974	3237302	en	East Reef						
4767975	3237304	en	North Rock						
4767976	3237305	en	North West Reef						
4767991	3237425	en	Flat Rock						
4767992	3237426	en	South West Grune						
1377308	3237429		La Grun de Lecq						
4767993	3237429		La Grune de Lecq						
4767994	3237455	en	Portinfer						
4767995	3237456	en	Plémont Deep						
2364388	3237464		Le Grand Becquet						
4767996	3237464	en	Grand Becquet						
2364389	3237466		Les Rocquettes						
2364390	3237466		Les Demies						
4767998	3237466	en	The Demies						
2364391	3237467		La Ville Bagot						
4767999	3237467	en	Ville Bagot						
2364392	3237469		L’île Agois						
4768000	3237469		Isle Agois						
15562793	3237472	wkdt	Q27118629						
16020512	3237472		Le Col de la Rocque						
1377309	3237475		La Plaine						
4768010	3237475	en	La Plaine Point						
15555035	3237475	wkdt	Q27118628						
4768094	3237489	en	Mourier Bay						
2364442	3237492		Le Lipendé						
4768095	3237492	en	La Lipende Point						
2364443	3237495		La Perruqe						
4768096	3237495		Perruque						
2364444	3237497		St. John’s						
4768097	3237497	en	Saint John						
8488041	3237497		St Jean						
8791143	3237497	link	https://en.wikipedia.org/wiki/Saint_John,_Jersey						
9069817	3237497	en	St John	1					
16766748	3237497	en	St. John						
4768098	3237499	en	Cormorant Rock						
4768099	3237503	en	Côtil Point						
2364445	3237504		La Demie						
4768100	3237504	en	Demie Rock						
4768101	3237506	en	Shamrock Bank						
2364446	3237515		Le Mont Mado						
2364447	3237515		Mount Mado						
4768107	3237515		Mont Mado						
1377311	3237518		Chevel Rock						
2364448	3237518		Le Cheval Guillaume						
4768108	3237518	en	Cheval Rock						
2364449	3237520		Le Long Êtchet						
4768109	3237520	en	Long Echet						
4768110	3237524	en	Vicard Harbour						
2364450	3237526		La Chrétienne						
4768111	3237526		La Grune						
4768112	3237530	en	Trinity						
8488033	3237530	fr	La Trinité						
8488034	3237530		La Trinneté						
8791144	3237530	link	https://en.wikipedia.org/wiki/Trinity,_Jersey						
2364451	3237531		L’Étacquerel						
4768113	3237531		L’Étaquerel						
4768114	3237532	en	Oyster Rocks						
2364452	3237533		Troupeurs						
4768115	3237533		Les Troupeurs						
2364453	3237536		La Demie de la Tour						
4768116	3237536		Demie de la Tour						
2364454	3237539		Le Nez du Guet						
4768117	3237539		Nez du Guet						
2364455	3237543		Pin Rock						
4768118	3237543	en	Hiaux						
2364456	3237548		Les Brayes						
4768119	3237548	en	Brayes Rocks						
4768120	3237550	en	Coupe Rock						
4768121	3237551	en	Le Graveur						
4768122	3237552	en	Pillon Rock						
4768123	3237553	en	Eureka						
2364457	3237554		Belval						
4768124	3237554	en	Belval Cove						
4768128	3237567	en	La Hau						
4768129	3237569	en	Joli						
4768133	3237589	en	Le Forêt						
4768134	3237590	en	Le Bût						
4768135	3237592	en	Frouquie						
4768136	3237593	en	Les Burons						
4768137	3237595	en	La Platte						
4768138	3237596	en	Frouquie						
4768139	3237600	en	Grune de South West						
4768141	3237606	en	La Grese						
4768142	3237608	en	Noire Roque						
4768143	3237611	en	La Joie						
4768144	3237614	en	Clump Rock						
4768146	3237615	en	Les Grunes						
4768147	3237620	en	Passe de l’Étoc						
4768149	3237625	en	Grune de North West						
4768150	3237632	en	Le Fierco						
4768151	3237634	en	L’Étoc						
4768152	3237635	en	Le Ruquet						
4768153	3237636	en	Grande Rousse						
4768154	3237640	en	Petite Rousse						
4768155	3237642	en	Marmotier						
15315990	3237642	wkdt	Q27118598						
4768157	3237646	en	Écrevière Bank						
4768162	3237716	en	Saint Martin						
7172688	3237716	link	https://en.wikipedia.org/wiki/Saint_Martin%2C_Jersey						
8488035	3237716	en	St Martîn	1					
8488036	3237716		Saint Martin de Grouville				1		
9394635	3237716	fr	Saint-Martin						
9394636	3237716	br	Saint Martîn						
9394637	3237716	nrm	Saint Martîn						
16766714	3237716	en	St. Martin						
4768164	3237717	en	Banc du Château						
4768165	3237720	en	Le Giffard						
4768166	3237724	en	Grune Le Feuvre						
4768167	3237726	en	La Grande Arconie						
4768168	3237727	en	Grunes du Gris-Banc						
4768169	3237729	en	Grune La Hauche						
4768174	3237823	en	Black Rock						
4768175	3237864	en	Saint Helier						
8488022	3237864	link	https://en.wikipedia.org/wiki/Saint_Helier						
8488023	3237864	fr	Saint-Hélier						
9069814	3237864	en	St Helier	1					
16766715	3237864	en	St. Helier						
1883169	6296595	icao	EGJJ						
1888340	6296595	iata	JER						
3032301	6296595	link	https://en.wikipedia.org/wiki/Jersey_Airport						
5894064	6296595	de	Flughafen Jersey						
5894065	6296595	fr	Aéroport de Jersey						
5894066	6296595	hu	Jersey-i repülőtér						
5894067	6296595	pl	Port lotniczy Jersey						
5894068	6296595	ru	Джерси						
11414154	6296595	es	Aeropuerto de Jersey						
11414155	6296595	it	Aeroporto di Jersey						
11414156	6296595	fa	فرودگاه جرزی						
11414157	6296595	ms	Lapangan Terbang Jersey						
11414158	6296595	tg	Фурудгоҳи ҷрзи						
11414159	6296595	vi	Sân bay Jersey						
13784026	6296595	unlc	JEJER						
15686032	6296595	wkdt	Q8991						
2369748	6640908		Le Chateau de Gros Nez						
5036316	6640908	en	Grosnez Castle						
7511958	6640908	link	https://en.wikipedia.org/wiki/Grosnez_Castle						
2369749	6640909		Pinnacle Rock						
5036331	6640909		Le Pinacle						
15242929	6640911	wkdt	Q27118590						
16046085	6640911		Les Laveurs						
2369750	6640912		La Petite Tour						
2369751	6640912		Martello Tower Number 1						
5036373	6640912		Lewis Tower						
2369752	6640913		Grosse Tour						
2369753	6640913		La Grosse Tour						
2369754	6640913		Martello Tower Number 2						
2369755	6640913		Kemp Tower						
5036374	6640913		Kempt Tower						
8685545	6640913	link	https://en.wikipedia.org/wiki/Kempt_Tower						
15708624	6640913	wkdt	Q15235459						
15519345	6640915	wkdt	Q27118589						
16046086	6640915		Le Val Rouget						
2369756	6640919		La Tour Carré						
5036375	6640919		Square Fort						
2369773	6641103		La Tour de la Rocque-Ho						
5036417	6641103		La Rocco Tower						
2369848	6641589		Val de la Mare						
2369849	6641589		Le Val de la Mare Reservoir						
5036492	6641589		Val de la Mare Reservoir						
2698209	6930747	en	The Savoy Jersey						
7198116	6947753	link	https://en.wikipedia.org/wiki/Trinity%2C_Jersey						
13784034	6947753	unlc	JETTY						
2729684	6950950		St Mary						
2730328	6950955	en	St Brelade	1					
8514812	7287755	en	St. Clements						
9137792	7287755	en	St Clement	1					
13784029	7287755	unlc	JESCJ						
16434058	7287755	link	https://en.wikipedia.org/wiki/St_Clement%27s,_Oxford						
16766416	7287755	en	Saint Clements						
15246029	7303885	wkdt	Q27118585						
9861381	9538861		St Peter's Village						
9861379	9538863	fr	Mont du Jubilé						
9861380	9538865		St. Ouen's Manor						
9861382	9538866		St. Peter's Arsenal						
10222733	9845475	en	Jersey Ferry Port						
10222734	9845475	en	Jersey Harbour						
11285223	10377080	en	St Quen			1			
10907179	10377087		Orchid Meadows						
11168555	10942508	link	https://en.wikipedia.org/wiki/Le_Hocq						
