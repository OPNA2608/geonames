cmake_minimum_required(VERSION 3.0)
project(geonames VERSION 0.3.0 LANGUAGES C)

include(GNUInstallDirs)
include(FindPkgConfig)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

pkg_check_modules(GIO REQUIRED gio-2.0>=2.32)
pkg_check_modules(GLIB REQUIRED glib-2.0>=2.32)

option(WANT_DOC "Build doc" ON)
option(WANT_TESTS "Build tests" ON)
option(WANT_DEMO "Build demo" OFF)

add_subdirectory(src)

if(WANT_DOC)
    add_subdirectory(doc)
endif()

if(WANT_TESTS)
    enable_testing()
    add_subdirectory(tests)
endif()

if(WANT_DEMO)
    add_subdirectory(demo)
endif()
