#!/bin/bash

set -e

PACKAGE="$1"
SRCDIR="$2"
DESTDIR="$3"

for po in ${SRCDIR}/po/*.po; do \
    export target="${DESTDIR}/locale/$(basename ${po} .po)/LC_MESSAGES"; \
    mkdir -p "${target}"; \
    msgfmt "${po}" -o "${target}/${PACKAGE}.mo"; \
done
